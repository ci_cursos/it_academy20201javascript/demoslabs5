import {connect, Schema, model} from 'mongoose';

async function main() {
    try {
        const url = 'mongodb://localhost:27017/testemongoose';
        const cliente = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('Conectado com sucesso');

        //Definir um schema
        const pessoaSchema = new Schema({
            nome: {type: String, required: true, minlength: 1, maxlength: 50},
            idade: {type: Number, required: true, min: 0}
        });

        //Definir um modelo
        const pessoaModel = model('Pessoa', pessoaSchema, 'pessoas');

        //Inserir um documento
        /*
        const pessoaDocument = new pessoaModel({nome:'John Doe', idade: 22});
        const pessoaDocumentInserido = await pessoaDocument.save();
        console.log('Inserido:');
        console.log(pessoaDocumentInserido);
        */
        //Consultar um documento
        /*
        const consulta = pessoaModel.where('idade').lt(30);
        const resutado = await consulta.exec();
        console.log('Busca:');
        console.log(resutado);
        */
        //Alterar um documento
        /*
        const umaPessoaDocument = await pessoaModel.findById('5f0df4e6ea90cc11c81607f3').exec();
        console.log('Busca:');
        console.log(umaPessoaDocument);
        umaPessoaDocument?.set('idade', 31);
        const umaPessoaDocumentAlterado = await umaPessoaDocument?.save();
        console.log(umaPessoaDocumentAlterado);
        */
        //Remover um documento
        const umaPessoaDocument = await pessoaModel.findById('5f0df4e6ea90cc11c81607f3').exec();
        const umaPessoaDocumentRemovido = await umaPessoaDocument?.remove();
        console.log('Removido:');
        console.log(umaPessoaDocumentRemovido);

        if (cliente && cliente.connection) {
            await cliente.connection.close();
            console.log('Desconectado');
        }
    } catch(erro) {
        console.log('Erro de acesso ao BD:');
        console.log(erro);
    }
}

main();